package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {

	welcome := "Welcome to userinput"
	fmt.Println(welcome)

	reader := bufio.NewReader(os.Stdin)

	println("Enter the rating for the movie:")

	//error ok //
	input, err := reader.ReadString('\n')
	fmt.Println("Thanks for rating ", input)
}
