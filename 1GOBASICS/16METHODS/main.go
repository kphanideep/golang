package main

import "fmt"

func main() {
	fmt.Println("Structs in golang")
	// no inheritance in golang; No super or parent

	phani := User{"phani", "phani@go.dev", true, 16}
	fmt.Println(phani)
	fmt.Printf("phani details are: %+v\n", phani)
	fmt.Printf("Name is %v and email is %v.\n", phani.Name, phani.Email)
	phani.GetStatus()
	phani.NewMail()
	fmt.Printf("Name is %v and email is %v.\n", phani.Name, phani.Email)

}

type User struct {
	Name   string
	Email  string
	Status bool
	Age    int
}

func (u User) GetStatus() {
	fmt.Println("Is user active: ", u.Status)
}

func (u User) NewMail() {
	u.Email = "test@go.dev"
	fmt.Println("Email of this user is: ", u.Email)
}
