package main

import "fmt"

func main() {
	fmt.Println("Structs in golang")
	// no inheritance in golang; No super or parent

	phani := User{"phani", "phani@go.dev", true, 16}
	fmt.Println(phani)
	fmt.Printf("phani details are: %+v\n", phani)
	fmt.Printf("Name is %v and email is %v.", phani.Name, phani.Email)

}

type User struct {
	Name   string
	Email  string
	Status bool
	Age    int
}
