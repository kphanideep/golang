package main

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	fmt.Println("Go Mysql Tutorial")

	db, err := sql.Open("mysql", "root:password@tcp(sql.hosting.com:3306)/testdb")

	if err != nil {
		panic(err.Error())

	}
	defer db.Close()

	insert, err := db.Query("Insert in to users")

	if err != nil {
		panic(err.Error())
	}

	defer insert.Close()
	fmt.Println("Sucessfully connected to MySQL databse")
}
