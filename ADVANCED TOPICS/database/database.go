package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
)

func main() {

	//connecting database
	db, err := sql.Open("mysql", "root:kamikaze@tcp(127.0.0.1:3306)/mytestdb")

	if err != nil {
		panic(err.Error())
	}

	defer db.Close()

	//creating table through SQL DDL commands
	_, err = db.Exec("CREATE TABLE IF NOT EXISTS mytestdb.emp(id int, name varchar(50), email varchar(50))")
	if err != nil {
		panic(err.Error())
	}

	//inserting 5 records
	result, err := db.Exec("INSERT INTO mytestdb.emp(id,name,email) VALUES (101,'mickey','mickey@gmail.com'),(102,'Donald','donald@gmail.com'),(103,'Tom','tom@gmail.com'),(104,'Jerry','jerry@gmail.com'),(105,'Minnie','minnie@gmail.com')")

	if err != nil {
		panic(err.Error())
	}

	rc, err := result.RowsAffected()
	if err != nil {
		panic(err.Error())
	}

	fmt.Printf("inserted %d rows\n", rc)

	//simple select query to fetch all records

	rows, err := db.Query("SELECT * FROM mytestdb.emp")
	if err != nil {
		panic(err.Error())
	}

	// use this struct to populate results returned from the database
	type emp struct {
		ID    int    
		Name  string 
		Email string 
	}

	for rows.Next() {
		var e emp
		err = rows.Scan(&e.ID, &e.Name, &e.Email)
		if err != nil {
			panic(err.Error())
		}
		fmt.Printf("%d %s %s\n", e.ID, e.Name, e.Email)
	}

} 