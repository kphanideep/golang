package main

import "fmt"

func main() {
	var conferenceName = "Go Conference"

	fmt.Println("Welcome to", conferenceName, "booking application")
	fmt.Println("Get Your Tickets to Attend")

}
