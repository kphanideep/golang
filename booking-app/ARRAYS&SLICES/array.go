package main

import "fmt"

func main() {
	// var conferenceName string = "Go Conference"
	conferenceName := "Go Conference"
	const conferenceTickets int = 100
	//we explicity specified here because remaingTickets should not be negative. ("uint")
	var remaingTickets uint = 100

    bookings := [] string{}

	fmt.Printf("Welcome to %v booking application\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v tickets are still available!\n", conferenceTickets, remaingTickets)
	fmt.Println("Get Your Tickets to Attend")

	// var bookings =[100]string {}

	var firstName string
	var lastName string
	var email string
	var userTickets uint
	//ask user for their name
	fmt.Println("Enter the userName:")
	fmt.Scan(&firstName)

	fmt.Println("Enter the lastName:")
	fmt.Scan(&lastName)

	fmt.Println("Enter the email address:")
	fmt.Scan(&email)

	fmt.Println("Enter no of tickets:")
	fmt.Scan(&userTickets)

	remaingTickets = remaingTickets - userTickets

	bookings = append(bookings, firstName+" "+lastName)

	fmt.Printf("The whole array : %v\n", bookings)
	fmt.Printf("Thank you %v %v for booking %v tickets.You will receive an confirmation email at %v \n", firstName, lastName, userTickets, email)
	fmt.Printf("%v tickets are still remaining for %v\n", remaingTickets, conferenceName)
}
