package main

import "fmt"

func main() {
	// var conferenceName string = "Go Conference"
	conferenceName := "Go Conference"
	const conferenceTickets int = 100
	//we explicity specified here because remaingTickets should not be negative. ("uint")
	var remaingTickets uint = 100

	fmt.Printf("Welcome to %v booking application\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v tickets are still available!\n", conferenceTickets, remaingTickets)
	fmt.Println("Get Your Tickets to Attend")

	var userName string
	var userTickets int
	//ask user for their name

	userName = "phani"
	userTickets = 4
	fmt.Printf("User %v has booked %v tickets.\n", userName, userTickets)

}
