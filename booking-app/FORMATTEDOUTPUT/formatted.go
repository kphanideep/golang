package main

import "fmt"

func main() {
	var conferenceName = "Go Conference"
	const conferenceTickets = 100
	var remaingTickets = 100

	fmt.Printf("Welcome to %v booking application\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v tickets are still available!\n", conferenceTickets, remaingTickets)
	fmt.Println("Get Your Tickets to Attend")

}
