package main

import "fmt"

func main() {
	// var conferenceName string = "Go Conference"
	conferenceName := "Go Conference"
	const conferenceTickets int = 100
	//we explicity specified here because remaingTickets should not be negative. ("uint")
	var remaingTickets uint = 100

	fmt.Printf("Welcome to %v booking application\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v tickets are still available!\n", conferenceTickets, remaingTickets)
	fmt.Println("Get Your Tickets to Attend")

	var firstName string
	var lastName string
	var email string
	var userTickets uint
	//ask user for their name
	fmt.Println("Enter the userName:")
	fmt.Scan(&firstName)

	fmt.Println("Enter the lastName:")
	fmt.Scan(&lastName)

	fmt.Println("Enter the email address:")
	fmt.Scan(&email)

	fmt.Println("Enter no of tickets:")
	fmt.Scan(&userTickets)

	remaingTickets = remaingTickets - userTickets

	fmt.Printf("Thank you %v %v for booking %v tickets.You will receive an confirmation email at %v \n", firstName, lastName, userTickets, email)
	fmt.Printf("%v tickets are still remaining for %v\n", remaingTickets, conferenceName)
}
