package main

import "fmt"

func main() {
	// Generics allow types to be parameters
	//Go 1.18+
	//
	//3 main features
	//1. Type parameter (with constraint)
	//2. Type inference
	//3. Type set
	fmt.Println(minInt(a:1, b:2))

}

func minInt(a T, b T ) int {
	if a<b {
		return a
	}
	return b
}
