package main

import (
	"fmt"
	"net/http"
)

func main() {
	links := []string{
		"http://google.com",
		"http://facebook.com",
		"http://cricbuz.om",
		"http://gmail.com",
		"http://amazon.com",
	}

	c := make (chan string)


	for _, link := range links {
		go checkLink(link)

	}
	fmt.Println(<- c)
}

func checkLink(link string,c chan string) {
	_, err := http.Get(link)
	if err != nil {
		fmt.Println(link, "might be down!")
		return
	}
	fmt.Println(link, "is up!")

}
