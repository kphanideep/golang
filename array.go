package main
import ("fmt")

func main() {
    func calcRemainderAndMod(numerator, denominator int) (int, int, error) {
        if denominator == 0 {
        return 0, 0, errors.New("denominator is 0")
        }
        return numerator / denominator, numerator % denominator, nil
       }
}

calcRemainderAndMod()
