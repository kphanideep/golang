package main

import (
	"fmt"
	"time"
)

func main() {
	go greeter("hello")
	greeter("world")
	//do not communicate by sharing memory instead shared memory by communicating.

}
func greeter(s string) {

	for i := 0; i < 6; i++ {
		time.Sleep(2 * time.Millisecond)
		fmt.Println(s)
	}

}

//go routine is simply created by adding a keyword GO.
