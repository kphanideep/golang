package controller

import (
	"context"
	"fmt"
	"log"
)

const connectionString = "mongodb+srv://phani:Phani@5579@cluster0.mfipw.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"

const dbName = "netflix"

const colName = "watchlist"

// MOST IMPORTANT

var collection *mongo.Collection

// connect with mongoDB

func init() {
	// client option
	clientOption := options.Client().ApplyURI(connectionString)

	//connect to mongodb
	//TODO returns a non-nil, empty Context. Code should use context.
	client, err := mongo.Connect(context.TODO(), clientOption)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("MongoDB Success")

	collection = client.Database(dbName).Collection(colName)
	//collection instance
	fmt.Println("Collection instance is ready")
}
